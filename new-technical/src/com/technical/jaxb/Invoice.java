package com.technical.jaxb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Invoice {
	@XmlElement
	private List<Customer> customers = new ArrayList<Customer>();
	private int total;
	private Date createDate;

	public Invoice() {
	}

	public Invoice(List<Customer> customers, int total, Date createDate) {
		this.customers = customers;
		this.total = total;
		this.createDate = createDate;
	}
	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public void addCustomer(Customer customer){
		this.getCustomers().add(customer);
	}

}
