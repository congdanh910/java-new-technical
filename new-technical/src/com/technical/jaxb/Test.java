package com.technical.jaxb;

import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class Test {
	
	public static void main(String[] args) throws JAXBException {
		Customer danh = new Customer("Danh", "Tan Phong - Cai Lay - Tien Giang", 27);
		Customer khanh = new Customer("Khanh", "Tan Phong - Cai Lay - Tien Giang", 26);
		Customer kiet = new Customer("Kiet", "Thu Dau 1 - Di An - Binh Duong", 27);
		Customer ky = new Customer("Ky", "Lam Dong", 27);
		
		//JAXBContext jaxbContext = JAXBContext.newInstance(Customer.class);
		//Marshaller marshaller = jaxbContext.createMarshaller();
		//marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		
		//marshaller.marshal(danh, System.out);
		
		Invoice invoice = new Invoice();
		invoice.addCustomer(danh); invoice.addCustomer(khanh);
		invoice.addCustomer(kiet); invoice.addCustomer(ky);
		invoice.setTotal(invoice.getCustomers().size());
		invoice.setCreateDate(new Date());
		
		JAXBContext invoiceContext = JAXBContext.newInstance(Invoice.class);
		Marshaller invoiceMarshaller = invoiceContext.createMarshaller();
		
		invoiceMarshaller.marshal(invoice, System.out);
	}
}
