package com.technical.jackson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Test {

	public static void main(String[] args) throws IOException {
		List<String> addresss = new ArrayList<String>();
		addresss.add("Tien Giang");
		addresss.add("Thanh Pho Ho Chi Minh");
		
		Staff staff = new Staff();
		staff.setName("Ho Cong Danh");
		staff.setAge(27);
		staff.setAddress(addresss);
		
		ObjectMapper objectMapper = new ObjectMapper();
		String generateToJson = objectMapper.writeValueAsString(staff);
		System.out.println("to json : " + generateToJson);
		System.out.println("convert json to object : " + objectMapper.readValue("", Staff.class));
	}

}
