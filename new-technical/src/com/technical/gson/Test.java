package com.technical.gson;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.model.Food;
import com.model.Soup;

public class Test {
	
	private static void basicTest(){
		Gson gson = new Gson();
		//Serialization
		System.out.println(gson.toJson(new int[]{1,2,3,4,5,6,7,8,9,0}));
		//Deserialization
		System.out.println(Arrays.toString(gson.fromJson("[1,2,3,4,5,6,7,8,9,0]", int[].class)));
	}
	
	private static void basicCollectionTest(){
		Gson gson = new Gson();
		Collection<Integer> ints = Arrays.asList(new Integer[]{1,2,3,4,5});
		//Serialization
		String json = gson.toJson(ints);
		System.out.println(json);
		//Deserialization
		Type collectionType = new TypeToken<Collection<Integer>>(){}.getType();
		Collection<Integer> ints2 = gson.fromJson(json, collectionType);
		System.out.println(ints2);
	}

	private static void testGenericClass(){
		Soup soup = new Soup(1, "Soup 1", "This is demo for Soup 1", new Date());
		Food<Soup> food = new Food<Soup>(soup);
		Gson gson = new Gson();
		//Serialization
		String json = gson.toJson(food);
		System.out.println(json);
		//Deserialization
		Type foodType = new TypeToken<Food<Soup>>() {}.getType();
		Food<Soup> food2 = gson.fromJson(json, foodType);
		System.out.println(food2.getType());
	}
	
	private static void testGsonBuilder(){
		Soup soup = new Soup(1, "Soup 1", "This is demo for Soup 1", new Date());
		Gson gson2 = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		System.out.println("gson2 : " + gson2.toJson(soup));
	}
	
	public static void main(String[] args) {
		System.out.println("=================basicTest==================");
		basicTest();
		System.out.println("=================basicTest==================\n");
		
		/*-------------------------------------------------------------------------*/
		System.out.println("=================basicCollectionTest==================");
		basicCollectionTest();
		System.out.println("=================basicCollectionTest==================\n");
		
		/*-------------------------------------------------------------------------*/
		System.out.println("=================testGenericClass==================");
		testGenericClass();
		System.out.println("=================testGenericClass==================\n");
	}

}
