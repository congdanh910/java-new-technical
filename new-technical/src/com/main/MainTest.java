package com.main;

import javax.inject.Inject;

import com.model.Student;

public class MainTest {
	
	@Inject
	private static Student student;

	public static void main(String[] args) {
		System.out.println(student.getAge());
		System.out.println(student.getAddress());
		System.out.println(student.getName());
	}

}
