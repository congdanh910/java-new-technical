package com.model;

public class Food<T> {
	private T type;

	public Food() {
	}

	public Food(T type) {
		this.type = type;
	}

	public T getType() {
		return type;
	}

	public void setType(T type) {
		this.type = type;
	}

}
