package com.model;

import java.util.Date;

import com.google.gson.annotations.Expose;

public class Soup {
	@Expose
	private int id;
	@Expose
	private String name;
	@Expose
	private String description;
	@Expose
	private Date createDate;

	public Soup() {
	}

	public Soup(int id, String name, String description, Date createDate) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.createDate = createDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
